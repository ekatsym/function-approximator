(defpackage function-approximator.config
  (:nicknames :fa.config)
  (:use :cl)
  (:import-from :mgl-mat
                #:*default-mat-ctype*)
  )
(in-package :fa.config)


(setf *default-mat-ctype* :float)
