(defpackage function-approximator.neural-network.backprop.unit.core
  (:nicknames :fa.nn.bp.unit.core)
  (:use :cl
        :fa.nn.bp.unit)
  (:import-from :fa.util
                #:defmacro!
                #:open-closure
                #:this
                #:with-open-closure)
  (:export #:unit
           #:defunit
           #:this
           #:with-unit
           #:unit-type?))
(in-package :fa.nn.bp.unit.core)


(defmacro! unit (args bindings &body clauses)
  `(lambda ,args
     (let ((,g!unit (open-closure ,bindings ,@clauses)))
       (funcall ,g!unit :initialize)
       ,g!unit)))

(defmacro! defunit (name args bindings &body clauses)
  `(defun ,name ,args
     (let ((,g!unit (open-closure ((unit-type ',name) ,@bindings) ,@clauses)))
       (funcall ,g!unit :initialize)
       ,g!unit)))

(defmacro with-unit (bindings unit &body body)
  `(with-open-closure ,bindings ,unit ,@body))

(defun unit-type? (unit unit-type)
  (with-unit ((ut unit-type)) unit
    (eq ut unit-type)))
