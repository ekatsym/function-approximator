(defpackage function-approximator.neural-network.backprop.unit.ms-loss
  (:nicknames :fa.nn.bp.unit.ms-loss)
  (:use :cl
        :fa.nn.bp.unit.core)
  (:import-from :fa.nn.bp.unit
                #:mean-squared-loss
                #:size
                #:output #:target #:loss
                #:dloss/dinput)
  (:import-from :mgl-mat
                #:mat
                #:make-mat
                #:copy!
                #:scal!
                #:axpy!
                #:nrm2
                #:dot
                #:m-
                #:fill!
                #:mat-dimensions))
(in-package :fa.nn.bp.unit.ms-loss)


(defunit
  mean-squared-loss (size)

  ((size          size)
   (output        (make-mat (list size 1)))
   (target        (make-mat (list size 1)))
   (loss          0.0)
   (loss2         (make-mat (list size 1)))
   (dloss/dinput  (make-mat (list 1 size))))

  (:initialize ()
   (fill! 0.0 output)
   (fill! 0.0 target)
   (setf loss 0.0)
   (fill! 0.0 dloss/dinput)
   'initialize)

  (:forward (x)
   (assert (equal (mat-dimensions x)
                  (list size 1)))
   (copy! x output))

  (:backward (tgt)
   (assert (equal (mat-dimensions tgt)
                  (list size 1)))
   (copy! tgt target)
   (copy! output dloss/dinput)      ; dL/dx <- y
   (axpy! -1.0 target dloss/dinput) ; dL/dx <- dL/dx - t
   )

  (:loss ()
   (setf loss
         (/ (dot dloss/dinput dloss/dinput)
            size 2))                        ; L <- 1/2 * (- y t)^2 / n
   ))
