(defpackage function-approximator.neural-network.backprop.unit.sce-loss
  (:nicknames :fa.nn.bp.unit.sce-loss)
  (:use :cl
        :fa.nn.bp.unit.core)
  (:import-from :fa.nn.bp.unit
                #:softmax-cross-entropy-loss
                #:size
                #:output #:target #:loss
                #:dloss/dinput)
  (:import-from :mgl-mat
                #:mat
                #:make-mat
                #:copy!
                #:mref
                #:.exp!
                #:.+!
                #:asum
                #:scal!
                #:axpy!
                #:dot
                #:.log!
                #:copy-mat
                #:fill!
                #:mat-dimensions))
(in-package :fa.nn.bp.unit.sce-loss)


(defunit
  softmax-cross-entropy-loss (size)

  ((size          size)
   (output        (make-mat (list size 1)))
   (target        (make-mat (list size 1)))
   (loss          0.0)
   (dloss/dinput  (make-mat (list 1 size))))

  (:initialize ()
   (fill! 0.0 output)
   (fill! 0.0 target)
   (setf loss 0.0)
   (fill! 0.0 dloss/dinput))

  (:forward (x)
   (assert (equal (mat-dimensions x)
                  (list size 1)))
   (copy! x output)                 ; y <- x
   (let ((max-x_i (mref x 0 0)))
     (dotimes (i size)
       (let ((x_i (mref x i 0)))
         (when (> x_i max-x_i)
           (setf max-x_i x_i))))
     (.+! (- max-x_i) output))      ; y <- y - max{x_i}
   (.exp! output)                   ; y <- exp(y)
   (scal! (/ (asum output)) output) ; y <- y / sum(y_i)
   )

  (:backward (tgt)
   (assert (equal (mat-dimensions tgt)
                  (list size 1)))
   (copy! tgt target)
   (copy! output dloss/dinput)      ; dL/dx <- y
   (axpy! -1.0 target dloss/dinput) ; dL/dx <- dL/dx - t
   )

  (:loss ()
   (setf loss (- (dot (.log! (copy-mat output))
                      target)))                 ; L <- - t * log(y)
   ))
