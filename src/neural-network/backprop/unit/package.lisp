(defpackage function-approximator.neural-network.backprop.unit
  (:nicknames :fa.nn.bp.unit)
  (:use :cl)
  (:import-from :fa.util
                #:this)
  (:export #:unit
           #:defunit
           #:this
           #:with-unit
           #:unit-type?

           ;; units
           #:affine #:relu #:sigmoid #:mean-squared-loss #:softmax-cross-entropy-loss

           ;; closure variables
           #:unit-type #:size
           #:input #:output #:target #:loss
           #:weight #:bias
           #:dloss/dinput #:doutput/dinput
           #:dloss/dweight #:dloss/dbias
           #:learning-rate #:weight-decay? #:weight-decay

           #:recurrent
           ))
