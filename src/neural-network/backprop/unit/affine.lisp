(defpackage function-approximator.neural-network.backprop.unit.affine
  (:nicknames :fa.nn.bp.unit.affine)
  (:use :cl
        :fa.nn.bp.unit.core)
  (:import-from :fa.nn.bp.unit
                #:affine
                #:size
                #:input #:output
                #:weight #:bias
                #:dloss/dinput #:dloss/dweight #:dloss/dbias
                #:learning-rate #:weight-decay?  #:weight-decay)
  (:import-from :mgl-mat
                #:mat
                #:make-mat
                #:copy!
                #:scal!
                #:axpy!
                #:gemm!
                #:fill!
                #:add-sign!
                #:mat-dimensions
                #:gaussian-random!))
(in-package :fa.nn.bp.unit.affine)


(defunit
  affine (input-size output-size &key initial-weight initial-bias (learning-rate 1.0e-4) weight-decay? (weight-decay 1.0e-3))

  ((size          (list input-size output-size))
   (input         (make-mat (list input-size 1)))
   (output        (make-mat (list output-size 1)))
   (weight        (make-mat (list output-size input-size)))
   (bias          (make-mat (list output-size 1)))
   (dloss/dinput  (make-mat (list 1 input-size)))
   (dloss/dweight (make-mat (list output-size input-size)))
   (dloss/dbias   (make-mat (list 1 output-size)))
   (learning-rate learning-rate)
   (weight-decay? weight-decay?)
   (weight-decay  weight-decay))

  (:initialize ()
   (if initial-weight
       (progn
         (assert (equal (mat-dimensions initial-weight)
                        (list output-size input-size)))
         (copy! initial-weight weight))
       (gaussian-random! weight))
   (if initial-bias
       (progn
         (assert (equal (mat-dimensions initial-bias)
                        (list output-size 1)))
         (copy! initial-bias bias))
       (gaussian-random! bias))
   (fill! 0.0 input)
   (fill! 0.0 output)
   (fill! 0.0 dloss/dweight)
   (fill! 0.0 dloss/dbias))

  (:forward (x)
   (assert (equal (mat-dimensions x)
                  (list input-size 1)))
   (copy! x input)
   (gemm! 1.0 weight input 0.0 output)  ; y <- W * x
   (axpy! 1.0 bias output)              ; y <- b + y
   )

  (:backward (dloss/doutput)
   (assert (equal (mat-dimensions dloss/doutput)
                  (list 1 output-size)))
   (gemm! 1.0 dloss/doutput input 1.0 dloss/dweight
          :transpose-a? t
          :transpose-b? t)                            ; dL/dw_ji <- dL/dy_j * x_i + dL/dw_ji
   (axpy! 1.0 dloss/doutput dloss/dbias)              ; dL/db <- dL/dy + dL/db
   (gemm! 1.0 dloss/doutput weight 0.0 dloss/dinput)  ; dL/dx <- dL/dy * W
   )

  (:update ()
   (when weight-decay?
     (axpy! weight-decay weight dloss/dweight))
   (axpy! (- learning-rate) dloss/dweight weight)
   (axpy! (- learning-rate) dloss/dbias bias)
   (fill! 0.0 dloss/dweight)
   (fill! 0.0 dloss/dbias)
   'updated)

  (:clear ()
   (fill! 0.0 dloss/dweight)
   (fill! 0.0 dloss/dbias)))
