(defpackage function-approximator.neural-network.backprop.unit.relu
  (:nicknames :fa.nn.bp.unit.relu)
  (:use :cl
        :fa.nn.bp.unit.core)
  (:import-from :fa.nn.bp.unit
                #:relu
                #:size
                #:output
                #:dloss/dinput #:doutput/dinput)
  (:import-from :mgl-mat
                #:mat
                #:make-mat
                #:.max!
                #:add-sign!
                #:geem!
                #:copy!
                #:fill!
                #:mat-dimensions))
(in-package :fa.nn.bp.unit.relu)


(defunit
  relu (size)

  ((size            size)
   (output          (make-mat (list size 1)))
   (doutput/dinput  (make-mat (list 1 size)))
   (dloss/dinput    (make-mat (list 1 size))))

  (:initialize ()
   (fill! 0.0 output)
   (fill! 0.0 doutput/dinput)
   (fill! 0.0 dloss/dinput))

  (:forward (x)
   (assert (equal (mat-dimensions x)
                  (list size 1)))
   (copy! x output)
   (.max! 0.0 output))

  (:backward (dloss/doutput)
   (assert (equal (mat-dimensions dloss/doutput)
                  (list 1 size)))
   (add-sign! 1.0 output
              0.0 doutput/dinput)
   (geem! 1.0 dloss/doutput doutput/dinput
          0.0 dloss/dinput)))
