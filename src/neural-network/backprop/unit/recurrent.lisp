(defpackage function-approximator.neural-network.backprop.unit.recurrent
  (:nicknames :fa.nn.bp.unit.rec)
  (:use :cl
        :fa.nn.bp.unit.util)
  (:import-from :fa.nn.bp.unit
                #:affine
                #:relu
                #:sigmoid
                #:softmax-cross-entropy-loss
                #:mean-squared-loss
                #:recurrent)
  (:import-from :mgl-mat
                #:mat
                #:make-mat
                #:copy!
                #:scal!
                #:axpy!
                #:gemm!
                #:fill!
                #:mat-dimensions)
  (:import-from :fa.util
                #:define-meta-closure))
(in-package :fa.nn.bp.unit.rec)


#|
(define-closure-template
  recurrent
  (input-size output-size activation &key (learning-rate 1.0e-4) weight bias rweight)
  ((modes           '(forward backward update loss size modes))
   (input-size      input-size)
   (output-size     output-size)
   (input           (make-mat (list input-size 1)))
   (output          (make-mat (list output-size 1)))
   (weight          (if weight
                        (progn (assert (equal (mat-dimensions weight)
                                              (list output-size input-size)))
                               weight)
                        (random-mat (list output-size input-size))))
   (bias            (if bias
                        (progn (assert (equal (mat-dimensions bias)
                                              (list output-size 1)))
                               bias)
                        (random-mat (list output-size 1))))
   (rweight         (if (progn (assert (equal (mat-dimensions rweight)
                                              (list output-size output-size)))
                               rweight)
                        (random-mat (list output-size output-size))))
   (dloss/dinput    (make-mat (list 1 input-size)))
   (dloss/dweight   (make-mat (list output-size input-size)
                              :initial-element 0.0))
   (dloss/dbias     (make-mat (list 1 output-size)
                              :initial-element 0.0))
   (dloss/drweight  (make-mat (list output-size output-size)
                              :initial-element 0.0))
   (learning-rate   learning-rate))
  (mode &optional mat)
  (case mode
    (forward
      (unit-missing-mat-assert mat)
      )
    (backward
      (unit-missing-mat-assert mat)
      )
    (update
      (axpy! (- learning-rate) dloss/dweight weight)
      (axpy! (- learning-rate) dloss/dbias bias)
      (fill! 0.0 dloss/dweight)
      (fill! 0.0 dloss/dbias)
      'update)
    (loss nil)
    (size (list input-size output-size))
    (modes modes)
    (otherwise (error 'unit-unknown-mode-error :datum mode))) 
  )
|#
