(defpackage function-approximator.neural-network.backprop.unit.sigmoid
  (:nicknames :fa.nn.bp.unit.sigmoid)
  (:use :cl
        :fa.nn.bp.unit.core)
  (:import-from :fa.nn.bp.unit
                #:sigmoid
                #:size
                #:output
                #:dloss/dinput)
  (:import-from :mgl-mat
                #:mat
                #:make-mat
                #:copy!
                #:.logistic!
                #:.square!
                #:scal!
                #:axpy!
                #:geem!
                #:mat-dimensions))
(in-package :fa.nn.bp.unit.sigmoid)


(defunit
  sigmoid (size)

  ((size          size)
   (output        (make-mat (list size 1)))
   (dloss/dinput  (make-mat (list 1 size))))

  (:forward (x)
   (assert (equal (mat-dimensions x)
                  (list size 1)))
   (copy! x output)
   (.logistic! output))

  (:backward (dloss/doutput)
   (assert (equal (mat-dimensions dloss/doutput)
                  (list size 1)))
   (copy! output dloss/dinput)            ; dL/dx <- y
   (.square! dloss/dinput)                ; dL/dx <- dL/dx ^2
   (axpy! -1.0 output dloss/dinput)       ; dL/dx <- dL/dx - y
   (geem! -1.0 dloss/doutput dloss/dinput
          0.0 dloss/dinput)               ; dL/dx <- dL/dy * dL/dx
   ))
