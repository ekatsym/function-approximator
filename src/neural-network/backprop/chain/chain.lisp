(defpackage function-approximator.neural-network.backprop.chain.chain
  (:nicknames :fa.nn.bp.chain.chain)
  (:use :cl
        :fa.nn.bp.unit
        :fa.nn.bp.chain)
  (:import-from :fa.util
                #:last1
                #:defmacro!
                #:open-closure
                )
  )
(in-package :fa.nn.bp.chain.chain)


(defmacro! chain (&rest units)
  `(open-closure ((,g!units (list ,@units)))

     (:forward (input)
      (reduce (lambda (x unit)
                (funcall unit :forward x))
              ,g!units
              :initial-value input))

     (:backward (target)
      (reduce (lambda (unit x)
                (funcall unit :backward x))
              ,g!units
              :initial-value target
              :from-end t))

     (:update ()
      (mapc (lambda (unit)
              (when (unit-type? unit 'affine)
                (funcall unit :update)))
            ,g!units))

     (:loss (input target)
      (funcall this :forward input)
      (funcall this :backward target)
      (mapc (lambda (unit)
              (when (unit-type? unit 'affine)
                (funcall unit :clear)))
            ,g!units)
      (funcall (last1 ,g!units) :loss))

     )
  )

#|
(let ((chain (chain
               (affine 3 4)
               (relu 4)
               (affine 4 2)
               (mean-squared-loss 2))))
  (values (funcall chain :forward (mgl-mat:make-mat '(3 1)))
          (funcall chain :forward (mgl-mat:make-mat '(3 1)))
          )
  )

(softmax-cross-entropy-loss 1)

(defun %chain (units unit-syms)
  (with-gensyms (run train update loss input output mode modes)
    (if (null units)
        `(closure
           ((,run
              (compose ,@(mapcar (lambda (u) `(partial ,u 'forward))
                                 unit-syms)))
            (,train
              (lambda (,input ,output)
                (funcall (compose ,@(mapcar (lambda (u) `(partial ,u 'forward))
                                            unit-syms))
                         ,input)
                (funcall (compose ,@(reverse
                                      (mapcar (lambda (u) `(partial ,u 'backward))
                                              unit-syms)))
                         ,output)))
            (,update
              (lambda ()
                ,@(mapcar (lambda (u) `(funcall ,u 'update))
                          unit-syms)))
            (,loss
              (partial ,(lastcar unit-syms) 'loss))
            (,modes
              (list 'run 'train 'update 'loss 'modes)))
           (,mode ,input ,output)
           (case ,mode
             (run (funcall ,run ,input))
             (train (funcall ,train ,input ,output))
             (update (funcall ,update))
             (loss (funcall ,loss))
             (modes ,modes)
             (error 'chain-unknown-mode-error :datum ,mode)))
        (let ((sym (gensym "CHAIN")))
          `(let ((,sym ,(first units)))
             ,(%chain (rest units) (cons sym unit-syms)))))))

(defmacro chain (&rest units)
  (%chain units '()))

(defmacro defchain (name &body units)
  (let ((sym (gensym "DEFCHAIN")))
    `(defclosure ,name ((,sym (chain ,@units))) (mode &optional input output)
       (funcall ,sym mode input output))))
|#
