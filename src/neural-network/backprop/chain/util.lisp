(defpackage function-approximator.neural-network.backprop.chain.util
  (:nicknames :fa.nn.bp.chain.util)
  (:use :cl))
(in-package :fa.nn.bp.chain.util)


(define-condition chain-unknown-mode-error (error)
  ((datum
     :initarg :datum
     :reader chain-unknown-mode-error-datum))
  (:report
    (lambda (o s)
      (format s "~&The symbol~%~2t~a~%is not ~%~2t~{~a~#[~;, or ~:;, ~]~}~%of modes.~%"
              (chain-unknown-mode-error-datum o)
              '(run train update loss modes)))))


#|
(chain
  (affine 2 1000)
  (relu 1000)
  (affine 1000 2)
  (softmax-cross-entropy-loss 2))

(defchain xor
  (affine 2 1000)
  (relu 1000)
  (affine 1000 2)
  (softmax-cross-entropy-loss 2))

(defparameter *xor-dataset*
  (list (list (make-mat '(2 1) :initial-contents '((0 0)))
              (make-mat '(2 1) :initial-contents '((1 0))))
        (list (make-mat '(2 1) :initial-contents '((0 1)))
              (make-mat '(2 1) :initial-contents '((0 1))))
        (list (make-mat '(2 1) :initial-contents '((1 0)))
              (make-mat '(2 1) :initial-contents '((0 1))))
        (list (make-mat '(2 1) :initial-contents '((1 1)))
              (make-mat '(2 1) :initial-contents '((1 0)))))) 

(time
  (loop repeat 1000 do
        (loop for dataset in *xor-dataset* do
              (progn (apply #'xor 'train dataset)
                     (xor 'update)))))
(xor 'run (make-mat '(2 1) :initial-contents '((0 0))))
(xor 'run (make-mat '(2 1) :initial-contents '((0 1))))
(xor 'run (make-mat '(2 1) :initial-contents '((1 0))))
(xor 'run (make-mat '(2 1) :initial-contents '((1 1))))


(defchain xor
  (affine 2 1000)
  (relu 1000)
  (affine 1000 1)
  (mean-squared-loss 1))

(defparameter *xor-dataset*
  (list (list (make-mat '(2 1) :initial-contents '((0 0)))
              (make-mat '(1 1) :initial-contents '((0))))
        (list (make-mat '(2 1) :initial-contents '((0 1)))
              (make-mat '(1 1) :initial-contents '((1))))
        (list (make-mat '(2 1) :initial-contents '((1 0)))
              (make-mat '(1 1) :initial-contents '((1))))
        (list (make-mat '(2 1) :initial-contents '((1 1)))
              (make-mat '(1 1) :initial-contents '((0)))))) 

(time
  (loop repeat 2000 do
        (loop for dataset in *xor-dataset* do
              (progn (apply #'xor 'train dataset)
                     (xor 'update)))))
(xor 'run (make-mat '(2 1) :initial-contents '((0 0))))
(xor 'run (make-mat '(2 1) :initial-contents '((0 1))))
(xor 'run (make-mat '(2 1) :initial-contents '((1 0))))
(xor 'run (make-mat '(2 1) :initial-contents '((1 1))))
|#
