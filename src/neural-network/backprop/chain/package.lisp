(defpackage function-approximator.neural-network.backprop.chain
  (:nicknames :fa.nn.bp.chain)
  (:use :cl)
  (:export #:chain
           #:defchain

           #:rchain
           #:defrchain
           ))
