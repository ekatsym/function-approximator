(defpackage function-approximator.neural-network.hebb.chains.init
  (:nicknames :fa.nn.hb.chains.init)
  (:use :cl)
  (:import-from :fa.util
                #:compose
                #:partial
                )
  (:import-from :mgl-mat
                #:mat)
  (:import-from :alexandria
                #:with-gensyms
                )
  )
(in-package :fa.nn.hb.chains.init)


