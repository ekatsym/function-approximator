(defpackage function-approximator.neural-network.hebb.units.init
  (:nicknames :fa.nn.hb.units.init)
  (:use :cl)
  (:export #:run
           #:run-with-update
           #:size
           #:modes))
(in-package :fa.nn.hb.units.init)
