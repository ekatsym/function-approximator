(defpackage function-approximator.neural-network.hebb.units.sigmoid
  (:nicknames :fa.nn.hb.units.sigmoid)
  (:use :cl
        :fa.nn.hb.units.init)
  (:import-from :mgl-mat
                #:mat
                #:make-mat
                #:copy!
                #:axpy!
                #:gemm!
                #:fill!
                #:.logistic!
                #:mat-dimensions)
  (:import-from :fa.util
                #:define-closure-template
                #:random-mat)
  (:export #:sigmoid))
(in-package :fa.nn.hb.units.sigmoid)


(define-closure-template
  sigmoid
  (size)
  ((modes   (list 'run 'run-with-update 'size 'modes))
   (size    size)
   (output  (make-mat (list size 1))))
  (mode &optional mat)
  (case mode
    (run
      (copy! mat output)
      (.logistic! output))
    (run-with-update
      (copy! mat output)
      (.logistic! output))
    (size size)
    (modes modes)))
