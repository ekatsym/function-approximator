(defpackage function-approximator.neural-network.hebb.units.affine
  (:nicknames :fa.nn.hb.units.affine)
  (:use :cl
        :fa.nn.hb.units.init)
  (:import-from :mgl-mat
                #:mat
                #:make-mat
                #:copy!
                #:axpy!
                #:gemm!
                #:fill!
                #:mat-dimensions)
  (:import-from :fa.util
                #:define-closure-template
                #:random-mat)
  (:export #:affine))
(in-package :fa.nn.hb.units.affine)


(define-closure-template
  affine
  (input-size output-size &key (learning-rate 1.0e-4) weight bias)
  ((modes         (list 'run 'run-with-update 'size 'modes))
   (input-size    input-size)
   (output-size   output-size)
   (input         (make-mat (list input-size 1)))
   (output        (make-mat (list output-size 1)))
   (weight        (if weight
                      (progn (assert (equal (mat-dimensions weight)
                                            (list output-size input-size)))
                             weight)
                      (random-mat (list output-size input-size))))
   (bias          (if bias
                      (progn (assert (equal (mat-dimensions bias)
                                            (list output-size 1)))
                             bias)
                      (random-mat (list output-size 1))))
   (learning-rate learning-rate))
  (mode &optional mat)
  (case mode
    (run
      (gemm! 1.0 weight mat 0.0 output)
      (axpy! 1.0 bias output)
      output)
    (run-with-update
      (copy! mat input)
      (gemm! 1.0 weight input 0.0 output)
      (axpy! 1.0 bias output)
      (gemm! learning-rate output input
             1.0 weight
             :transpose-b? t)
      (axpy! learning-rate output bias)
      output)
    (size (list input-size output-size))
    (modes modes)))
