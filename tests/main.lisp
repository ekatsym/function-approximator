(defpackage function-approximator/tests/main
  (:use :cl
        :function-approximator
        :rove))
(in-package :function-approximator/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :function-approximator)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
