(defsystem "function-approximator"
  :version "0.1.0"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :depends-on ("mgl-mat")
  :components ((:module "src"
                :serial t
                :components
                ((:file "util")
                 (:file "config")
                 (:module "neural-network"
                  :serial t
                  :components
                  ((:module "backprop"
                    :serial t
                    :components
                    ((:module "unit"
                      :serial t
                      :components
                      ((:file "package")
                       (:file "core")
                       (:file "affine")
                       (:file "relu")
                       (:file "sigmoid")
                       (:file "ms-loss")
                       (:file "sce-loss")))
                     (:module "chain"
                      :serial t
                      :components
                      ((:file "package")
                       ;(:file "util")
                       (:file "chain")
                       ))))
                   ;(:module "hebb"
                   ; :serial t
                   ; :components
                   ; ((:module "units"
                   ;   :serial t
                   ;   :components
                   ;   ((:file "init")
                   ;    (:file "affine")
                   ;    (:file "sigmoid")
                   ;    (:file "package")))
                   ;  (:module "chains"
                   ;   :serial t
                   ;   :components
                   ;   ((:file "init")))))
                   )))))
  :description ""
  :long-description
  #.(read-file-string
      (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "function-approximator/tests"))))

(defsystem "function-approximator/tests"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :depends-on ("function-approximator"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for function-approximator"

  :perform (test-op (op c) (symbol-call :rove :run c)))
